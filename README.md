
# Bichon

Script for responding to ical calendar invites. Adapted from asciiphil's
[mutt-ical][1] scripts.

## Configure

The script replies via email. For this, it contains several constants
that need configuring. This is done via an ini file, by default it looks
in `~/.config/bichon/settings.ini`. The `-s` flag can be used to specify
other settings files.

The config file may contain multiple profiles. The format of a profile
is

    [<profile_name>]
    EmailAddress = <email>
    SmtpServer = <url>
    SmtpPort = <port>
    Username = <username>
    PasswordCommand=<command>
    XOAuth2=<yes|no>

By default, the profile `DEFAULT` is used. You can specify a different
profile with the `-p` flag.

`PasswordCommand` should be a shell command that returns your password.
(E.g. runs a password manager.) If this command takes arguments,
separate them with a comma.

    PasswordCommand = getpass,email_pass

The `XOAuth2` setting is optional and assumed to be no. That is, the default is
basic username/password login. For XOAuth2 login, use a script such as
[mutt_oauth2.py][mutt_oauth2py] ([README][mutt_oauth2readme]) to obtain the
oauth token as the `PasswordCommand` and set `XOAuth2` to yes.

[mutt_oauth2py]: https://gitlab.com/muttmua/mutt/-/blob/master/contrib/mutt_oauth2.py
[mutt_oauth2readme]: https://gitlab.com/muttmua/mutt/-/blob/master/contrib/mutt_oauth2.py.README

## Usage

An email invite with ical attachment is read from stdin. The
yes/no/maybe response is passed via a command line argument with the
`-r` flag.

    bichon.py -r yes

You can add a comment to your response with `-c`

    bichon.py -r no -c "sorry, will be otherwise engaged"

## Usage with Mutt

The script can be used with Mutt by adding a macro that uses the
`pipe-message` command to send an email invite through Bichon.

I use

    macro pager A ":exec pipe-message<enter>bichon.py -r"

which is not a complete command. When `A` is pressed the partial bichon
command is shown and you can complete it with yes/no/maybe. `enter` will
then send the response email.

## Constructing the Response

The interesting code showing the format of the response is commented

    # Build response calendar

I *think* you can just get away with building an ICS response file and
attaching it to any email. Bichon constructs an email with content type

    text/calendar; charset="utf-8"; method="REPLY"

but i'm not sure this is necessary.

[1]: https://github.com/asciiphil/mutt-ical 
