#!/usr/bin/env python

from __future__ import print_function

import datetime
import email
import email.utils
import argparse
import re
import smtplib
import subprocess
import sys
import configparser

import icalendar  # https://pypi.python.org/pypi/icalendar

from email.mime.text import MIMEText


ICALENDAR_PRODID = '-//net.chilon.bichon//1.0'
ICALENDAR_CONTENT_TYPES = ['text/calendar', 'application/ics']
ICALENDAR_REPLY_STATUSES = {
    'yes': 'ACCEPTED',
    'no': 'DECLINED',
    'maybe': 'TENTATIVE',
}

DEFAULT_CONFIG = "~/.config/bichon/settings.ini"

parser = argparse.ArgumentParser()
parser.add_argument('-r', '--reply', type=str,
                    choices=ICALENDAR_REPLY_STATUSES.keys(),
                    help='Give your reply to a request for attendance.')
parser.add_argument('-s', '--settings', type=str,
                    help='Path to settings.ini, defaults to ~/.config/bichon/settings.ini.')
parser.add_argument('-p', '--profile', type=str,
                    help='Which profile in the settings file to use, defaults to DEFAULT.')
parser.add_argument('-c', '--comment', type=str,
                    help='Optionally add a comment to the reply.')
parser.add_argument('--cc', action='store_true',
                    help='Send a copy of the reply to yourself.')
parser.add_argument('--refresh', action='store_true',
                    help='Request an updated copy of the given event.')
parser.add_argument('--acceptcounter', action='store_true',
                    help='Accept a counter offer for time')
parser.add_argument('-d', '--debug', action='store_true',
                    help='Enable debugging; do not actually send email.')

options = parser.parse_args()
if not (options.reply or options.refresh or options.acceptcounter):
    print('You must specify an action.', file=sys.stderr)
    exit(1)

config_file = DEFAULT_CONFIG
if options.settings:
    config_file = options.settings

config = configparser.ConfigParser()
config.read(config_file)

profile_name = 'DEFAULT'
if options.profile:
    profile_name = options.profile
if profile_name not in config:
    print("Profile", profile_name, "not found in", config_file)
    exit(1)

profile = config[profile_name]

for field in ['EmailAddress', 'SmtpServer', 'SmtpPort', 'Username', 'PasswordCommand']:
    if field not in profile:
        print(field, "not found in profile", profile_name, "in", config_file)
        exit(1)

email_address = profile['EmailAddress']
smtp_server = profile['SmtpServer']
smtp_port = profile['SmtpPort']
username = profile['Username']
password_command = profile['PasswordCommand'].split(',')
use_oauth = False
if 'XOAuth2' in profile and profile['XOAuth2'].upper() == "YES":
    use_oauth = True

mailto_address = 'mailto:' + email_address

def copy_prop(prop, source, dest):
    if prop in source:
        dest.add(prop, source[prop])

def copy_props(source, dest):
    for prop in source:
        dest.add(prop, source[prop])

def find_icalendar(email):
    global ICALENDAR_CONTENT_TYPES
    for part in email.walk():
        if part.get_content_type() in ICALENDAR_CONTENT_TYPES:
            return part
    return None

def get_source_event(request):
    source_event = None
    for component in request.subcomponents:
        if component.name == 'VEVENT':
            if source_event:
                print('Cannot generate reply; more than one event in source calendar.',
                      file=sys.stderr)
                exit(1)
            source_event = component
    if not source_event:
        print('Cannot generate reply; no events in source calendar.',
              file=sys.stderr)
        exit(1)
    return source_event

def get_attendee(request):
    source_event = get_source_event(request)
    attendee = None
    if isinstance(source_event['ATTENDEE'], list):
        for a in source_event['ATTENDEE']:
            if a.lower() == mailto_address.lower():
                attendee = a
    elif source_event['ATTENDEE'].lower() == mailto_address.lower():
        attendee = source_event['ATTENDEE']
    if not attendee:
        attendee = icalendar.prop.vCalAddress(email_address)
        print(
            f'Warning: Email "{email_address}" is not listed as an attendee,'
                + ' Adding anyway...',
            file=sys.stderr
        )
    return attendee

def build_request_response(request):
    source_event = get_source_event(request)
    attendee = get_attendee(request)

    response_icalendar = icalendar.Calendar()
    if options.reply:
        response_icalendar['METHOD'] = 'REPLY'
    elif options.refresh:
        response_icalendar['METHOD'] = 'REFRESH'
    response_icalendar['VERSION'] = '2.0'
    response_icalendar['PRODID'] = ICALENDAR_PRODID
    response_icalendar['CALSCALE'] = 'GREGORIAN'

    reply_event = icalendar.Event()
    reply_event['ATTENDEE'] = attendee
    if options.reply:
        reply_event['ATTENDEE'].params['PARTSTAT'] = ICALENDAR_REPLY_STATUSES[options.reply]
        reply_event['ATTENDEE'].params['X-NUM-GUESTS'] = '1'
    reply_event.add('DTSTAMP', datetime.datetime.now())
    copy_prop('ORGANIZER', source_event, reply_event)
    copy_prop('RECURRENCE-ID', source_event, reply_event)
    copy_prop('UID', source_event, reply_event)
    if options.reply:
        copy_prop('SEQUENCE', source_event, reply_event)
    copy_prop('DTSTART', source_event, reply_event)
    copy_prop('DTEND', source_event, reply_event)
    if options.comment:
        reply_event['COMMENT'] = options.comment.strip()

    response_icalendar.add_component(reply_event)

    return response_icalendar

def get_request_response_email(request):
    source_event = get_source_event(request)
    organizer_address = re.sub(
        'mailto:', '', source_event['ORGANIZER'], flags=re.IGNORECASE
    )
    if 'CN' in source_event['ORGANIZER'].params:
        reply_to = email.utils.formataddr(
            (source_event['ORGANIZER'].params['CN'], organizer_address)
        )
    else:
        reply_to = organizer_address
    return reply_to

def build_counter_response(request):
    source_event = get_source_event(request)

    response_icalendar = icalendar.Calendar()
    response_icalendar['METHOD'] = 'REQUEST'
    response_icalendar['VERSION'] = '2.0'
    response_icalendar['PRODID'] = ICALENDAR_PRODID
    response_icalendar['CALSCALE'] = 'GREGORIAN'

    reply_event = icalendar.Event()
    copy_props(source_event, reply_event)
    reply_event['SEQUENCE'] = str(int(source_event['SEQUENCE']) + 1)
    response_icalendar.add_component(reply_event)

    return response_icalendar

def build_sasl_string(user, bearer_token):
    """Assumes XOAuth2, see mutt_oauth2.py for other examples"""
    return f'user={user}\1auth=Bearer {bearer_token}\1\1'

###
# Find the iCalendar in the source email and parse it.
source_email = email.message_from_file(sys.stdin)
source_icalendar = find_icalendar(source_email)
if not source_email:
    print('No iCalendar found to reply to.', file=sys.stderr)
    exit(1)
request = icalendar.Calendar.from_ical(source_icalendar.get_payload(decode=True))

###
# Verification of the validity of the requestion action relative to the supplied
# iCalendar.

if options.reply and not 'METHOD' in request:
    print('Cannot generate reply; source calendar was not a REQUEST.',
          file=sys.stderr)
    exit(1)

reply_to = None

if request['METHOD'] == 'REQUEST' and (options.reply or options.refresh):
    response_icalendar = build_request_response(request)
    reply_to = get_request_response_email(request)
elif request['METHOD'] == 'COUNTER' and options.acceptcounter:
    response_icalendar = build_counter_response(request)
    reply_to = source_email['From']
else:
    print('No action specified for METHOD', request['METHOD'])
    exit(1)

###
# Build response email.

source_event = get_source_event(request)

response_email = MIMEText(response_icalendar.to_ical(), 'calendar', 'UTF-8')
response_email.set_param('method', response_icalendar['METHOD'])
response_email['From'] = email_address
response_email['To'] = reply_to

subj_prefix = 'Re'
if options.reply:
    subj_prefix = ICALENDAR_REPLY_STATUSES[options.reply]

if 'SUMMARY' in source_event:
    response_email['Subject'] = subj_prefix + ': ' + source_event['SUMMARY']
else:
    response_email['Subject'] = subj_prefix + ': ' + source_event['UID']
response_email['Date'] = email.utils.formatdate(localtime=True)

if options.debug:
    print(response_email.as_string(), end='')
else:
    parsed_reply_to = email.utils.parseaddr(reply_to)
    if parsed_reply_to == ('', ''):
        destination = reply_to
    else:
        destination = parsed_reply_to[1]
    s = smtplib.SMTP(smtp_server, smtp_port)
    password = subprocess.check_output(password_command) \
                         .decode("ascii") \
                         .strip()
    s.connect(smtp_server, smtp_port)
    s.ehlo()
    s.starttls()
    s.ehlo()
    if use_oauth:
        sasl_string = build_sasl_string(username, password)
        s.auth("XOAUTH2", lambda _=None: sasl_string)
    else:
        s.login(username, password)

    print(response_email.as_string())

    destinations = [destination]
    if options.cc:
        destinations.append(email_address)

    s.sendmail(email_address, destinations, response_email.as_string())

    s.quit()
